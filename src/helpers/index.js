import {EMAIL_REGEXP} from "../constants";

export const validator = (schema, values) => {
    const errors = {};
    if (Array.isArray(schema)) {
        schema.forEach((field) => {
            checker(errors, field, values);
        });
    } else {
        checker(errors, schema, values);
    }

    return errors;
}

const checker = (errors, field, values) => {
    field.validators?.forEach((name) => {
        switch (name) {
            case 'required':
                if (!values[field.name]) {
                    errors[field.name] = 'Field is required!';
                }
                break;
            case 'email':
                if (!EMAIL_REGEXP.test(values[field.name])) {
                    errors[field.name] = 'Email is not valid!';
                }
                break;
            default:
                break;
        }
    });
}