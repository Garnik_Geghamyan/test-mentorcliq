import { lazy } from 'react';
const Registration = lazy(() => import('../views/pages/Registration/Registration'));
const Dashboard = lazy(() => import('../views/pages/Dashboard/Dashboard'));
const Profile = lazy(() => import('../views/pages/Profile/Profile'));

const APP_ROUTES = [
    {
        path: '/',
        component: Registration,
        exact: true,
        private: false,
    },
    {
        path: '/dashboard',
        component: Dashboard,
        exact: true,
        private: false,
    },
    {
        path: '/profile',
        component: Profile,
        exact: true,
        private: false,
    }
];

export { APP_ROUTES };
