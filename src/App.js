import React, { Suspense } from 'react';
import { Route, BrowserRouter as Router, Routes } from 'react-router-dom';
import {APP_ROUTES} from "./routes/AppRoutes";

import MainLayout from "./views/components/MainLayout/MainLayout";
import { GlobalStyles } from "./global.styled";

function App() {

    return (
        <div className="main">
            <GlobalStyles />
            <Router>
                <Suspense fallback={null}>
                    <MainLayout>
                        <Routes>
                            <React.Fragment>
                                {APP_ROUTES.map((route) =>
                                    (
                                        <Route
                                            key={route.path}
                                            path={route.path}
                                            exact={route.exact}
                                            element={<route.component />}
                                        />
                                    ),
                                )}
                            </React.Fragment>
                        </Routes>
                    </MainLayout>
                </Suspense>
            </Router>
        </div>
    );
}

export default App;
