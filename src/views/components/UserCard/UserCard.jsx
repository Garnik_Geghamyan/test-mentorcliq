import React from 'react';

import { UserCardStyled } from "./UserCard.styled";

export default function UserCard({ onAction, data }) {
    return(
        <UserCardStyled onClick={onAction(data)}>
            <div className="UserCard-item">
                <div className="UserCard-item--title">Name:</div>
                <div>{data.first_name}</div>
            </div>
            <div className="UserCard-item">
                <div className="UserCard-item--title">Last Name:</div>
                <div>{data.last_name}</div>
            </div>
            <div className="UserCard-item">
                <div className="UserCard-item--title">Email:</div>
                <div>{data.email}</div>
            </div>
        </UserCardStyled>
    );
}