import styled from 'styled-components';

export const UserCardStyled = styled.div`
  display: flex;
  flex-direction: column;
  border: 1px solid black;
  border-radius: 10px;
  cursor: pointer;
  margin-right: 10px;
  padding: 10px;
  margin-bottom: 16px;

  &:hover {
    background: #efeeee;
  }

  .UserCard-item {
    display: flex;
    border-bottom: 1px solid #e3e3e3;
    padding: 5px 0;

    .UserCard-item--title {
      width: 90px;
    }
  }
`;
