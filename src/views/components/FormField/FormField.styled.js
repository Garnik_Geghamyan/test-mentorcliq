import styled from 'styled-components';

export const FormFieldStyled = styled.div`
  display: flex;
  flex-direction: column;
  margin-bottom: 20px;

  select {
    width: 100%;
    min-width: 15ch;
    max-width: 270px;
    border: 1px solid black;
    border-radius: 0.25em;
    padding: 0.25em 0.5em;
    font-size: 1.25rem;
    cursor: pointer;
    line-height: 1.1;
    background-color: #fff;
    background-image: linear-gradient(to top, #f9f9f9, #fff 33%);
  }

  input {
    width: 100%;
    min-width: 15ch;
    max-width: 250px;
    border: 1px solid black;
    border-radius: 0.25em;
    padding: 0.25em 0.5em;
    font-size: 1.25rem;
    line-height: 1.1;
    background-color: #fff;
    background-image: linear-gradient(to top, #f9f9f9, #fff 33%);
  }

  .FormField-error {
    color: #f82d2d;
  }

`;