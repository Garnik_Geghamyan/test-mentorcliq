import React from 'react';

import { FormFieldStyled } from "./FormField.styled";

export default function FormField({ onFieldChange, field, value, error }) {

    const handleChange = (event) => {
        onFieldChange(field, event.target.value);
    }

    const renderFormField = () => {
        switch (field.type) {
            case 'select':
                return (
                    <select id={field.name} onChange={handleChange} value={value || ''}>
                        <option key="empty" value=""/>
                        {field.options?.map((item, index) => (
                            <option value={item} key={index}>{item}</option>
                        ))}
                    </select>
                );
            default:
                return (
                    <input id={field.name} type="text" onChange={handleChange} value={value || ''} />
                );
        }
    }

    return (
        <FormFieldStyled>
            <label htmlFor={field.name}>{field.label}</label>
            {renderFormField()}
            {error && <span className="FormField-error">{error}</span>}
        </FormFieldStyled>
    );
}