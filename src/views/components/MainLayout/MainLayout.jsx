import React from 'react';
import { useSelector } from "react-redux";
import { userSel } from "../../../store/user";

import Header from "../Header/Header";

export default function MainLayout({ children }) {
    const userData = useSelector(userSel.userDataSelector);

    return(
        <div>
            {userData && <Header />}
            {children}
        </div>
    );
}