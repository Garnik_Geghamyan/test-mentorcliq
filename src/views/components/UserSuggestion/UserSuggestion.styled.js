import styled from 'styled-components';

export const UserSuggestionStyled = styled.div`
  .UserSuggestion-cards-wrapper {
    display: flex;
    flex-wrap: wrap;
  }
  
  .UserSuggestion-action {
    margin-top: 24px;
    button {
      padding: 10px 20px;
      background: transparent;
      border: 1px solid black;
      border-radius: 4px;
      cursor: pointer;
      margin-right: 10px;
    }
    
    .submit {
      background: #378989;
      color: #fff;
    }
  }
`;