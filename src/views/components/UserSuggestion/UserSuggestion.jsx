import React, { useState, useEffect } from 'react';
import { useDispatch, useSelector } from "react-redux";
import { MAX_SUGGESTIONS_COUNT } from "../../../constants";
import { userOp, userSel } from "../../../store/user";

import { UserSuggestionStyled } from "./UserSuggestion.styled";
import UserCard from "../UserCard/UserCard";

export default function UserSuggestion({ userData, onSubmit, onCancel, readOnly }) {
    const dispatch = useDispatch();
    const suggestions = useSelector(userSel.suggestionsSelector);
    const matches = useSelector(userSel.matchesSelector);

    const [selectedSuggestions, setSelectedSuggestions] = useState(suggestions);
    const [selectedMatches, setSelectedMatches] = useState([]);

    useEffect(() => {
        dispatch(userOp.getMatches(userData, suggestions));
        // eslint-disable-next-line
    }, []);

    useEffect(() => {
        setSelectedMatches(matches);
    }, [matches, dispatch]);

    const handleSelectMatches = (data) => () => {
        if (selectedSuggestions.length !== MAX_SUGGESTIONS_COUNT) {
            setSelectedSuggestions([...selectedSuggestions, data]);
            setSelectedMatches(selectedMatches.filter(item => item.email !== data.email));
        }
    }

    const handleSubmit = () => {
        onSubmit(selectedSuggestions);
    }

    const handleDeleteSuggestion = (data) => () => {
        if (!readOnly) {
            setSelectedMatches([...selectedMatches, data]);
            setSelectedSuggestions(selectedSuggestions.filter(item => item.email !== data.email));
        }
    }

    return (
        <UserSuggestionStyled>
            <div className="UserSuggestion-title">
                <h3>User Suggestions</h3>
            </div>
            <div className="UserSuggestion-cards-wrapper">
                {
                    selectedSuggestions?.map((item) => {
                        return (
                            <UserCard data={item} onAction={handleDeleteSuggestion} key={item.email} />
                        );
                    })
                }
            </div>
            {
                !readOnly && <>
                    <div className="UserSuggestion-title">
                        <h3>User Matches</h3>
                    </div>
                    <div className="UserSuggestion-cards-wrapper">
                        {
                            selectedMatches?.map((item) => {
                                return (
                                    <UserCard data={item} onAction={handleSelectMatches} key={item.email} />
                                );
                            })
                        }
                    </div>
                    <div className="UserSuggestion-action">
                        <button onClick={onCancel}>Back</button>
                        <button className="submit" onClick={handleSubmit}>Submit</button>
                    </div>
                </>
            }
        </UserSuggestionStyled>
    );
}