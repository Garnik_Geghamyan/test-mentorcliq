import styled from 'styled-components';

export const HeaderStyled = styled.div`
  height: 32px;
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  padding: 12px 24px;
  background: #378989;

  a {
    text-decoration: none;
    color: #fff;
  }

  .Header-navigation {
    display: flex;
    justify-content: center;
    align-items: center;
    
    a {
      margin-right: 8px;
    }
  }
  
  .Header-logout {
    display: flex;
    justify-content: center;
    align-items: center;
  }

`;