import React, { useCallback } from 'react';
import { Link } from "react-router-dom";
import { useDispatch } from "react-redux";
import { userOp } from "../../../store/user";

import { HeaderStyled } from './Header.styled';

export default function Header() {
    const dispatch = useDispatch();

    const handleLogout = useCallback(() => {
        dispatch(userOp.logout());
    }, [dispatch]);

    return(
        <HeaderStyled>
            <div className="Header-navigation">
                <Link to="/dashboard">Dashboard</Link>
                <Link to="/profile">Edit Profile</Link>
            </div>
            <div  className="Header-logout">
                <Link to="/" onClick={handleLogout}>Logout</Link>
            </div>
        </HeaderStyled>
    );
}