import React from 'react';
import { useSelector } from "react-redux";
import { userSel } from "../../../store/user";

import UserSuggestion from "../../components/UserSuggestion/UserSuggestion";
import { DashboardStyled } from "./Dashboard.styled";

export default function Dashboard() {
    const userData = useSelector(userSel.userDataSelector);

    return(
        <DashboardStyled>
            <div>
                <h2>Hi {userData?.first_name}</h2>
            </div>
            <UserSuggestion readOnly userData={userData} />
        </DashboardStyled>
    );
}