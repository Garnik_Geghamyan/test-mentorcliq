import React from 'react';
import { useDispatch, useSelector } from "react-redux";
import { useNavigate } from "react-router-dom";
import { userOp, userSel } from "../../../store/user";

import UserSuggestion from "../../components/UserSuggestion/UserSuggestion";
import { ProfileStyled } from "./Profile.styled";


export default function Profile() {
    const dispatch = useDispatch();
    const navigate = useNavigate();

    const userData = useSelector(userSel.userDataSelector);

    const handleEditProfile = (suggestions) => {
        dispatch(userOp.setSuggestions(suggestions));
        navigate('/dashboard');
    }

    const handleCancel = () => {
        navigate('/dashboard');
    }

    return(
        <ProfileStyled>
            <div>
                <h2>
                    Profile {userData?.first_name}
                </h2>
            </div>
            <UserSuggestion userData={userData} onSubmit={handleEditProfile} onCancel={handleCancel}/>
        </ProfileStyled>
    );
}