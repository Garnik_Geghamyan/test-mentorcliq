import React from 'react';
import { PERSONAL_INFO_FORM_FIELDS } from "../../../../constants";

import FormField from "../../../components/FormField/FormField";

export default function PersonalInfo({ onFieldChange, formData, errors, onSubmit }) {

    return (
        <div className="Registration-form--wrapper">
            <div className="Registration-form--title">
                <h2>Personal Information</h2>
            </div>
            <form className="PersonalInfo-form" onSubmit={onSubmit}>
                {PERSONAL_INFO_FORM_FIELDS.map((item) => (
                    <FormField value={formData[item.name]}
                               field={item}
                               error={errors[item.name]}
                               onFieldChange={onFieldChange}
                               key={item.name} />
                ))}
                <div className="Registration-form--submit">
                    <input type="submit" value="Next" />
                </div>
            </form>
        </div>
    );
}