import React from 'react';
import { JOB_INFO_FORM_FIELDS } from "../../../../constants";

import FormField from "../../../components/FormField/FormField";

export default function JobInfo({ onFieldChange, formData, onSubmit, errors, onCancel }) {
    return (
        <div className="Registration-form--wrapper">
            <div className="Registration-form--title">
                <h2>Job Information</h2>
            </div>
            <form className="JobInfo-form" onSubmit={onSubmit}>
                {JOB_INFO_FORM_FIELDS.map((item) => (
                    <FormField value={formData[item.name]}
                               field={item}
                               error={errors[item.name]}
                               onFieldChange={onFieldChange}
                               key={item.name}
                    />
                ))}
                <div className="Registration-form--submit">
                    <button onClick={onCancel}>Back</button>
                    <input type="submit" value="Next" />
                </div>
            </form>
        </div>
    );
}