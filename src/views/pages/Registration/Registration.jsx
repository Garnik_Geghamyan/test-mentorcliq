import React, {useEffect, useState} from 'react';
import {useDispatch, useSelector} from "react-redux";
import { useNavigate } from 'react-router-dom';
import {userOp, userSel} from "../../../store/user";
import { validator } from "../../../helpers";
import {
    JOB_INFO_FORM_FIELDS,
    PERSONAL_INFO_FORM_FIELDS,
    REGISTRATION_FORM_STEPS
} from "../../../constants";

import PersonalInfo from "./components/PersonalInfo";
import JobInfo from "./components/JobInfo";
import UserSuggestion from "../../components/UserSuggestion/UserSuggestion";
import { RegistrationStyled } from "./Registration.styled";

export default function Registration() {
    const dispatch = useDispatch();
    const navigate = useNavigate();
    const userData = useSelector(userSel.userDataSelector);

    const [currentStep, setCurrentStep] = useState(REGISTRATION_FORM_STEPS.FIRST_STEP);
    const [formData, setFormData] = useState({});
    const [errors, setErrors] = useState({});

    useEffect(() => {
        if (userData) {
            navigate('/dashboard');
        }
    }, [userData, navigate]);

    const handleFieldChange = (field, value) => {
        const data = {
            ...formData,
            [field.name]: value,
        }
        setFormData(data);
        setErrors(validator(field, data));
    };

    const handlePersonalInfoSubmit = (event) => {
        event.preventDefault();
        let errors = validator(PERSONAL_INFO_FORM_FIELDS, formData);
        if (Object.keys(errors).length) {
            setErrors(errors);
        } else {
            setCurrentStep(REGISTRATION_FORM_STEPS.SECOND_STEP);
        }
    }

    const handleJobInfoSubmit = (event) => {
        event.preventDefault();
        let errors = validator(JOB_INFO_FORM_FIELDS, formData);
        if (Object.keys(errors).length) {
            setErrors(errors);
        } else {
            setCurrentStep(REGISTRATION_FORM_STEPS.THIRD_STEP);
        }
    }

    const handleComplete = (suggestions) => {
        dispatch(userOp.registry(formData));
        dispatch(userOp.setSuggestions(suggestions));
    }

    const handleCancel = (step) => () => {
        setCurrentStep(step);
    }

    const renderCurrentStep = () => {
        switch (currentStep) {
            case REGISTRATION_FORM_STEPS.FIRST_STEP:
                return (
                    <PersonalInfo
                        formData={formData}
                        errors={errors}
                        onFieldChange={handleFieldChange}
                        onSubmit={handlePersonalInfoSubmit}
                    />
                );
            case REGISTRATION_FORM_STEPS.SECOND_STEP:
                return (
                    <JobInfo
                        formData={formData}
                        errors={errors}
                        onSubmit={handleJobInfoSubmit}
                        onFieldChange={handleFieldChange}
                        onCancel={handleCancel(REGISTRATION_FORM_STEPS.FIRST_STEP)}
                    />
                );
            case REGISTRATION_FORM_STEPS.THIRD_STEP:
                return (
                    <UserSuggestion
                        userData={formData}
                        onSubmit={handleComplete}
                        onCancel={handleCancel(REGISTRATION_FORM_STEPS.SECOND_STEP)}
                    />
                );
            default:
                return null;
        }
    }

    return(
        <RegistrationStyled>
            {renderCurrentStep()}
        </RegistrationStyled>
    );
}