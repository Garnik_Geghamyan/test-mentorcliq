import styled from 'styled-components';

export const RegistrationStyled = styled.div`
  padding: 24px;
  
  .Registration-form--wrapper {
    width: 400px;
    margin: 0 auto;
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: center;
  }
  
  .Registration-form--submit {
    display: flex;
    align-items: center;
    justify-content: center;
    
    input {
      padding: 10px 20px;
      border: 1px solid black;
      border-radius: 4px;
      cursor: pointer;
      background: #378989;
      color: #fff;
    }

    button {
      padding: 10px 20px;
      background: transparent;
      border: 1px solid black;
      border-radius: 4px;
      margin-right: 8px;
      cursor: pointer;
    }
  }
`;
