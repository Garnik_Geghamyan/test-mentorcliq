const suggestionsSelector = (state) => state.user.suggestions;

const userDataSelector = (state) => state.user.userData;

const matchesSelector = (state) => state.user.matches;

export const userSel = {
    suggestionsSelector,
    userDataSelector,
    matchesSelector,
};
