import { userSlice } from './userSlice';
import EmployeesList from '../../_mock_/employees.json';

const getMatches = (userData, suggestions) => {
    const { setMatches } = userSlice.actions;

    return async (dispatch) => {
        try {
            let matches= [];
            EmployeesList.forEach((employer) => {
                if (
                    employer.department === userData.department ||
                    employer.city === userData.city ||
                    employer.job_title === userData.job_title ||
                    employer.country === userData.country
                ) {
                    if (!suggestions.find(item => item.email === employer.email)) {
                        matches.push(employer);
                    }
                }
            });
            dispatch(setMatches(matches));
        } catch (err) {
        }
    };
};

const registry = (userData) => {
    const { setUserData } = userSlice.actions;

    return async (dispatch) => {
        try {
            dispatch(setUserData(userData));
            localStorage.setItem('user', JSON.stringify(userData));
        } catch (err) {
        }
    };
};

const setSuggestions = (suggestions) => {
    const { setSuggestions } = userSlice.actions;

    return async (dispatch) => {
        try {
            dispatch(setSuggestions(suggestions));
            localStorage.setItem('suggestions', JSON.stringify(suggestions));
        } catch (err) {
        }
    };
};

const logout = () => {
    const { setSuggestions, setUserData } = userSlice.actions;

    return async (dispatch) => {
        try {
            dispatch(setSuggestions([]));
            dispatch(setUserData(null));
            localStorage.clear();
        } catch (err) {
        }
    };
};

export const userOp = {
    getMatches,
    registry,
    setSuggestions,
    logout,
};
