export function initialGlobalState() {
    return {
        suggestions: localStorage.getItem('suggestions') ? JSON.parse(localStorage.getItem('suggestions')) : [],
        matches: [],
        userData: localStorage.getItem('user') ? JSON.parse(localStorage.getItem('user')) : null,
    };
}
