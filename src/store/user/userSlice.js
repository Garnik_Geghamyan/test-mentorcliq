import { createSlice } from '@reduxjs/toolkit';
import { initialGlobalState } from './initialState';

export const userSlice = createSlice({
    name: 'user',
    initialState: initialGlobalState(),
    reducers: {
        setSuggestions(state, action) {
            state.suggestions = action.payload;
        },
        setMatches(state, action) {
            state.matches = action.payload;
        },
        setUserData(state, action) {
            state.userData = action.payload;
        },
    },
});
