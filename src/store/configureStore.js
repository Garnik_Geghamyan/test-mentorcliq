import { reducers } from './reducers';
import { configureStore } from '@reduxjs/toolkit';
import ReduxThunk from 'redux-thunk';

import * as Constants from '../constants';
import preloadedState from './preloadedState';

export function Store() {
    const middleware = [ReduxThunk];

    const store = configureStore({
        reducer: reducers(),
        middleware,
        devTools: Constants.IS_DEVELOPMENT,
        enhancers: [],
        preloadedState,
    });

    return store;
}
