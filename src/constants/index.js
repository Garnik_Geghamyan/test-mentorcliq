const getEnvironment = (() => process.env.NODE_ENV)();
export const IS_DEVELOPMENT = getEnvironment !== 'production';
export const MAX_SUGGESTIONS_COUNT = 5;
export const REGISTRATION_FORM_STEPS = {
    FIRST_STEP: 'personal-info',
    SECOND_STEP: 'job-info',
    THIRD_STEP: 'suggestion',
}

// eslint-disable-next-line
export const EMAIL_REGEXP = RegExp(/^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i);

export const PERSONAL_INFO_FORM_FIELDS = [
    {
        name: 'first_name',
        label: 'First Name',
        type: 'text',
        validators: ['required'],
    },
    {
        name: 'last_name',
        label: 'Last Name',
        type: 'text',
        validators: ['required'],
    },
    {
        name: 'email',
        label: 'Email',
        type: 'text',
        validators: ['required', 'email'],
    },
    {
        name: 'gender',
        label: 'Gender',
        type: 'select',
        options: ['Female','Male'],
        validators: ['required'],
    },
];

export const JOB_INFO_FORM_FIELDS = [
    {
        name: 'department',
        label: 'Department',
        type: 'select',
        options: ['Marketing','Support','Human Resources','Product Management','Services','Legal','Accounting','Sales'],
        validators: ['required'],
    },
    {
        name: 'job_title',
        label: 'Job Title',
        type: 'select',
        options: ['Analog Circuit Design manager','Web Designer II','Health Coach II','Senior Editor','Nuclear Power Engineer','Account Coordinator','Geological Engineer','Recruiting Manager','Speech Pathologist','Account Representative IV','Research Nurse','Systems Administrator III','Help Desk Technician','Senior Developer','Engineer IV','Computer Systems Analyst IV','Safety Technician I','Design Engineer','Programmer I','Compensation Analyst'],
        validators: ['required'],
    },
    {
        name: 'country',
        label: 'Country',
        type: 'select',
        options: ['China','Japan','Armenia','Norway','Russia','Uganda','Egypt','Portugal','Honduras','Nigeria','Finland','Indonesia'],
        validators: ['required'],
    },
    {
        name: 'city',
        label: 'City',
        type: 'select',
        options: ['Damaying','Hirakata','Sevan','Drammen','Nytva','Entebbe','Cherkasskoye','Jinzhuang','Isnā','Foros de Salvaterra','Lyalichi','Marale','Dishnā','Gusau','Ortiga','Kanshi','Varkaus','Nobo','Pedrogão','Changfeng'],
        validators: ['required'],
    },
];